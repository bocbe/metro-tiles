﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Metro_Tiles
{
    class Class1
    {
        public Class1()
        {

        }
        public void setWidth(decimal w)
        {
            width = w;
        }
        public void setHeight(decimal h)
        {
            height = h;
        }
        public void setInput(string i)
        {
            input = i;
            setCoords();
            setLetters();
        }

        void setCoords()
        {
            int i = 0;
            int x1 = 0;
            int y1 = 0;
            while (i < input.Length)
            {
                if (input[i] == '.') 
                {
                    x.Add(x1);
                    y.Add(y1);
                    x1++;
                }
                else if (input[i] == '\n') { }
                else if (input[i] == '\r')
                {
                    x1 = 0;
                    y1++;
                }
                else
                {
                    x.Add(x1);
                    x1++;
                    y.Add(y1);
                }
                i++;
            }
        }

        void setLetters()
        {
            int i = 0;
            while (i < input.Length)
            {
                if (input[i] != '\r' && input[i] != '\n')
                {
                    letters += input[i];
                }
                i++;
            }
        }

        string setBlock(int i, char letter)
        {
            int h=0;
            int w=0;
            int j=i;
            string block;
            if (j>=(int)width && letters[j] == (letters[j-(int)width])) return "";
            while (letters[j] != '.' && letters[j] == letter)
            {
                w++;
                j++;
                if (j >= letters.Length) break;
            }
            j = i;
            while (letters[j] != '.' && j<letters.Length)
            {
                h++;
                j += ((int)width);
                if (j >= letters.Length) break;
            }
            block = w + "x" + h;
            return block;
        }


        public string getOutput()
        {
            int i = 0;
            string block;
            while (i < letters.Length)
            {
                if (letters[i] != '.')
                {
                    block = setBlock(i, letters[i]);
                    if (block != "") output += block + " tile of character " + letters[i] + " at" + '(' + x[i] + ',' + y[i] + ')' + '\r' + '\n';
                    while (letters[i] != '.')
                    {
                        i++;
                    }
                }
                if (i<letters.Length) i++;
            }
            return output;
        }

      
        decimal width;
        decimal height;
        string input;
        string letters;
        string output;
        ArrayList x = new ArrayList();
        ArrayList y = new ArrayList();
        

    }
}

