﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Metro_Tiles
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Class1 myObject;
            myObject = new Class1();


            myObject.setWidth(numericUpDown1.Value);
            myObject.setHeight(numericUpDown2.Value);
            myObject.setInput(textBox1.Text);

            textBox4.Text = myObject.getOutput();
            textBox4.Refresh();
        }

    }
}
